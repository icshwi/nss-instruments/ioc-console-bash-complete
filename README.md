# ioc-console-bash-complete
Based on input from 
https://iridakos.com/tutorials/2018/03/01/bash-programmable-completion-tutorial.html

**How to install**

Source the script to make it work without installing, e.g. `source console-autocomp.bash`

Add the script to `$/etc/bash_completion.d/` to make it available for all users

**How to use**

`$ console <tab><tab>` # double tab starts the autocomplete, continue to type and double tab to complete the IOC name